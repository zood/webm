module StringExtensions
  refine String do
    def pluralize(quantity)
      return self if quantity < 2
      self.concat 's'
    end
  end
end
