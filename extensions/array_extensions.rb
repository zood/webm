# frozen_string_literal: true

module ArrayExtensions
  refine Array do
    def single
      raise "Expected #{self} to be of size 1" if size > 1
      first
    end

    def each_with_reverse_index
      (0...length).each do |i|
        yield self[i], (length - i)
      end
    end
  end
end
