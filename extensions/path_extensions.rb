# frozen_string_literal: true

require "pathname"

module PathExtensions
  refine Pathname do
    def same_dir?(other)
      self.expand_path.dirname == other.expand_path.dirname
    end
  end
end
