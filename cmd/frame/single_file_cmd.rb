# frozen_string_literal: true

module Frame
  class SingleFileCmd < Successor
    def initialize(input_file)
      @input_file = input_file
    end

    def call
      args = Args.new(@input_file, out_file)

      args.call { system(*args) }.success? or exit 1
      with_real_success { out_file.summary("Frame") }
    end

    def out_file
      @out_file ||=
        WEBMFile.new("frame", @input_file.id)
    end
  end
end
