# frozen_string_literal: true

module Frame
  module BlinkFrame
    def args
      ["ffmpeg", "-y", "-loglevel", "error", *_unmute,
      "-i", "file:#{@image}", "-lossless", "1",
      *VideoFilterBuilder.to_arg, "-shortest", @out_file]
    end
  end
end
