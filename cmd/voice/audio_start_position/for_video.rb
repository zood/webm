# frozen_string_literal: true

module AudioStartPosition
  class ForVideo
    def initialize(audio_span, video_ffprobe)
      @audio_span = audio_span
      @video_duration = video_ffprobe.duration
    end

    def to_arg
      ["-ss", start_position] if start_position > 0
    end

  private

    def start_position
      case
      when $opt.span.start_at_beginning
        0
      when $opt.span.stop_at_end
        ss_to_keep_the_end
      when implicit_audio_start_position?
        ss_to_stop_at_exact_point
      else
        @audio_span.start_position.to_f
      end
    end

    def implicit_audio_start_position?
      @audio_span.start_position.to_f.zero?
    end

    def ss_to_stop_at_exact_point
      @audio_span.stop_position.to_f - @video_duration
    end

    def ss_to_keep_the_end
      @audio_span.seconds - @video_duration
    end
  end
end
