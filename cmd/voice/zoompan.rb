# frozen_string_literal: true

class Zoompan
  def initialize(spot:,
                 speed:,
                 duration:,
                 width:,
                 height:)
    @spot = spot
    @speed = speed
    @duration = duration
    @width = width
    @height = height
  end

  def to_s
    [prescale, zoompan].join(",")
  end

  private

  def prescale
    # https://trac.ffmpeg.org/ticket/4298
    format("scale=%sx%s", @width * mult, @height * mult)
  end

  def mult
    (5000 / [@width, @height].max).clamp(7..)
  end

  def zoompan
    (+"zoompan=").tap do |s|
      s << "z='zoom+#{@speed}'"
      s << zoom_coordinates(@spot)
      s << ":d=#{@duration}"
      s << ":s=#{dimensions}"
    end
  end

  def dimensions
    "#{@width}x#{@height}"
  end

  def zoom_coordinates(spot)
    {
      1 => ":x='x':y='if(gte(zoom,1.5),y,y+1)'",
      2 => ":x='iw/2-(iw/zoom/2)':y='if(gte(zoom,1.5),y,y+1)'",
      3 => ":x='if(gte(zoom,1.5),x,x+1)':y='if(gte(zoom,1.5),y,y+1)'",
      4 => ":x='x':y='ih/2-(ih/zoom/2)'",
      5 => ":x='iw/2-(iw/zoom/2)':y='ih/2-(ih/zoom/2)'",
      6 => ":x='if(gte(zoom,1.5),x,x+1)':y='ih/2-(ih/zoom/2)'",
      7 => ":x='if(gte(zoom,1.5),x,x-1)':y='y'",
      8 => ":x='iw/2-(iw/zoom/2)':y='y'",
      9 => ":x='if(gte(zoom,1.5),x,x+1)':y='y'",
    }.fetch(spot)
  end
end
