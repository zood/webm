# frozen_string_literal: true

module Info
  class SingleFileCmd
    def initialize(input_file)
      @input_file = input_file
    end

    def call
      report_metadata
    end

    def report_metadata
      puts FileSummary.string(
        filename: @input_file.ls_color,
        filesize: Color.fs[@input_file.size],
        metadata: source_metadata,
      )
    end

  private

    def source_metadata
      return unless (title = @input_file.title)
      "\n#{' ' * 4}" + Color.dark[title]
    end
  end
end
