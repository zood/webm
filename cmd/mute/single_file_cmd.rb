# frozen_string_literal: true

module Mute
  class SingleFileCmd < Successor
    using PathExtensions

    def initialize(input_file)
      @input_file = input_file
      configure_metadata
    end

    def call
      mute_args.call do
        system(*mute_args)
      end.success? or exit 1

      report_success
    end

  private

    def configure_metadata
      Metadata.video = @input_file.title
      Metadata.filepath = @input_file.path
    end

    def report_success
      with_real_success do
        out_file.summary("Mute")
        out_file.rename to: result
      end
    end

    def result
      return @input_file if same_dir?
      Pathname.pwd.join(@input_file.basename)
    end

    def same_dir?
      out_file.path.same_dir?(@input_file.path)
    end

    def out_file
      @out_file ||= OutFile.new(
        @input_file.basename,
        ext: @input_file.ext,
        dir: "/tmp"
      )
    end

    def mute_args
      @mute_args ||= Args.new(
        @input_file,
        out_file.path
      )
    end
  end
end
