# frozen_string_literal: true

module Mute
  class ReplacingSingleFileCmd < KeepingSingleFileCmd
    using PathExtensions

    def report_success
      with_real_success do
        out_file.summary("Mute")
        out_file.rename to: result
      end
    end

    def out_file
      @out_file ||= OutFile.new(
        @input_file.basename,
        ext: @input_file.ext,
        dir: "/tmp"
      )
    end

  private

    def _mute_interval
      %w[-an]
    end

    def result
      return @input_file if same_dir?
      Pathname.pwd.join(@input_file.basename)
    end

    def same_dir?
      out_file.path.same_dir?(@input_file.path)
    end
  end
end
