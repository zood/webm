# frozen_string_literal: true

require_relative "../../extensions/path_extensions"
require_relative "single_file_cmd"
require_relative "../../lib/role/input_files_iterator"
require_relative "cmd"
require_relative "args"
