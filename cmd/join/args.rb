# frozen_string_literal: true

module Join
  class Args < DryRun
    def initialize(files_list, out_file)
      populate do |j|
        j.push(
          "ffmpeg", "-y", "-loglevel", "error", "-f", "concat",
          "-safe", "0", "-i", files_list.out_file.path, "-c", "copy",
          *Metadata.to_arg, out_file
        )
      end
    end
  end
end
