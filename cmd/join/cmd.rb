# frozen_string_literal: true

module Join
  class Cmd < Successor
    using Identifiable

    def initialize(input_files)
      allowed_files_count = $opt.cycle > 1 ? (1..) : (2..)

      @input_files = input_files.apply(
        Rule.new.limit_amount allowed_files_count
      )
      check_codecs_compatibility if allowed_files_count.begin > 1
    end

    def call
      configure_metadata
      joiner = Args.new(files_list, out_file)
      joiner.call { system(*joiner) }.success? or exit 1
      with_real_success { out_file.summary("Join") }
    end

    def out_file
      @out_file ||= out_file_type.new(
        "join",
        IO.read(files_list.out_file.path).id
      )
    end

  private

    def out_file_type
      @input_files.map { |f| File.extname(f) }
        .uniq
        .then { |exts| OutFilePicker.call exts }
    end

    def configure_metadata
      case (meta_titles = files_list.meta_titles).size
      when 1
        Metadata.video = meta_titles.last
      when (2..)
        Metadata.edit_bunch meta_titles
      end
    end

    def files_list
      @files_list ||=
        FilesList.new(*(@input_files * $opt.cycle))
    end

    def check_codecs_compatibility
      return if $opt.dry_run

      audio_codecs = []
      video_codecs = []

      @input_files.each do |file|
        audio_codecs << (file.audio_codec or "<nil>")
        video_codecs << (file.video_codec or "<nil>")
      end

      unless audio_codecs.uniq!&.one?
        throw :halt, format(
          "%s: %s",
          Color.err["Incompatible audio codecs"],
          audio_codecs.join(", ")
        )
      end

      unless video_codecs.uniq!&.one?
        throw :halt, format(
          "%s: %s",
          Color.err["Incompatible video codecs"],
          video_codecs.join(", ")
        )
      end
    end
  end
end
