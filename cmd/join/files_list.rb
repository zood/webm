# frozen_string_literal: true

module Join
  class FilesList
    def initialize(*input_files)
      @input_files = input_files
      File.open(out_file.path, "w") { |f| f.write content_string }
    end

    def out_file
      @out_file ||= Tempfile.new(["join_list", ".txt"])
    end

    def meta_titles
      @input_files.map { |file| file.title }.uniq.compact
    end

  private

    def content_string
      @input_files.map do |f|
        "file #{Shellwords.escape f.path.expand_path}"
      end.join("\n")
    end
  end
end
