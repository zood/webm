# frozen_string_literal: true

module Title
  class SingleFileCmd < Successor
    attr_accessor :shared_data

    def initialize(input_file)
      @input_file = input_file
    end

    def call
      configure_metadata
      return unless title_changed?

      args = Args.new(@input_file, temp_out_file.path)
      args.call { system(*args) }.success? or exit 1

      with_real_success do
        temp_out_file.rename to: out_file
        out_file.summary("Title")
      end
    end

  private

    def title_changed?
      return true if @input_file.title != Metadata.to_s

      warn format(
        "%s: %s",
        Color.warn["Title unchanged, ommit"],
        out_file
      )
      false
    end

    def configure_metadata
      Metadata.video = @input_file.title
      Metadata.filepath = @input_file.path

      case $opt.meta.edit
      when :single then Metadata.edit_single
      when :bunch
        return if Metadata.edited_title
        Metadata.edit_bunch shared_titles.call
      end
    end

  alias_method :shared_titles, :shared_data

    def out_file
      @out_file ||= OutFile.new(
        @input_file.path,
        ext: @input_file.ext
      )
    end

    def temp_out_file
      @temp_out_file ||= OutFile.new(
        "titled",
        ext: @input_file.ext,
        dir: "/tmp"
      )
    end
  end
end
