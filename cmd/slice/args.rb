# frozen_string_literal: true

module Slice
  class Args < DryRun
    def initialize(input_file, span, video_filter, out_file)
      populate do |c|
        c.push(
          "ffmpeg", "-y", "-loglevel", "error", "-nostdin",
          "-ss", span.start_position, "-i", input_file,
          "-t", span.seconds, *video_filter, "-vsync", "vfr", out_file
        )
      end
    end
  end
end
