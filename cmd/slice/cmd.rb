# frozen_string_literal: true

module Slice
  class Cmd < Successor
    def initialize(input_files)
      @input_file = input_files.apply(
        Rule.new.limit_amount(1..1)
      )
    end

    def call
      $opt.take_nth_frame&.then { extract_only_nth_frame(_1) }
      grayscale if $opt.grayscale
      print Color.dur[format("%0.01f sec ", span.duration.seconds)]
      slice_args = Args.new(@input_file, span, video_filter, out_file)
      slice_args.call { system(*slice_args) }.success? or exit 1
      with_real_success do
        count = Dir['*.png'].count
        fps = (count / span.duration.seconds).round
        printf("%s frames %s %s\n", count, Color.abr["fps: #{fps}"],
               Color.vbr["delay: #{100.fdiv(fps).round(1)}"])
      end
    rescue Interrupt
      exit 1
    end

  private

    def grayscale
      video_filter << "format=gray"
    end

    def extract_only_nth_frame(n)
      video_filter << "select=not(mod(n\\,#{n}))"
    end

    def video_filter
      @video_filter ||= VideoFilterBuilder.new(
        input_file: @input_file,
        span: span,
        crop: crop,
        restrained_dimensions: restrained_dimensions,
        pixelize: pixelize,
        do_not_sharpen: true,
      ).call
    end

    def restrained_dimensions
      limited_width = [crop.width, @input_file.width].compact.min
      limited_height = [crop.height, @input_file.height].compact.min
      if limited_width <= limited_height
        limited_width = [limited_width, $opt.short_side_limit, $opt.width].compact.min
        limited_height = [limited_height, $opt.long_side_limit, $opt.height].compact.min
      else
        limited_width = [limited_width, $opt.long_side_limit, $opt.width].compact.min
        limited_height = [limited_height, $opt.short_side_limit, $opt.height].compact.min
      end
      original_width, original_height = @input_file.width, @input_file.height
      if $opt.rotate&.size&.odd?
        limited_width, limited_height = limited_height, limited_width
        original_width, original_height = original_height, original_width
      end
      RestrainedDimensions.new(
        width: RestrainedWidth.new(
          original: original_width,
          explicit_limit: nil,
          implicit_limit: limited_width,
          crop_limit: crop.width
        ),
        height: RestrainedHeight.new(
          original: original_height,
          explicit_limit: nil,
          implicit_limit: limited_height,
          crop_limit: crop.height
        )
      )
    end

    def pixelize
      Struct.new(:active?, keyword_init: true).new(active?: false)
    end

    def crop
      @crop ||=
        case
        when $opt.crop.active?
          crop = ($opt.crop.value || PositionsStore.crop) or throw :halt, "Crop is unspecified"
          Crop.new(Area.from_xywh(crop))
        when $opt.detect_crop
          DetectCrop::Cmd.new(
            input_file: @input_file,
            ffprobe: @input_file.ffprobe,
            start_position: span.start_position.to_f,
            stop_position: span.stop_position.to_f,
          ).call
        else
          Struct.new(:width, :height, :x_offset, :y_offset).new
        end
    end

    def span
      @span ||= Span.new(Edges.new @input_file, @input_file.duration)
    end

    def out_file
      @out_file ||= OutFile.new("slice", @input_file.id, "%03d", ext: ".png")
    end
  end
end
