# frozen_string_literal: true

module Convert
  class Cmd
    include SharedInputFilesIterator

    def initialize(input_files)
      @input_files = input_files.apply(
        Rule.new(
          avi: 0..,
          flv: 0..,
          mkv: 0..,
          mov: 0..,
          mp4: 0..,
          webm: 0..,
          wmv: 0..,
        ).limit_amount(1..)
      )
    end

  private

    def shared_data
      # dummy
    end
  end
end
