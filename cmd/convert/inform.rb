# frozen_string_literal: true

module Convert
  class Inform
    def initialize(**args)
      @args = args
    end

    def display
      warn(
        Array.new.tap do |d|
          d << file_size
          d << video_bitrate
          d << audio_bitrate unless $opt.audio_bitrate.boring?
          d << video_dimensions
          d << duration
          d << pixel_size if @args[:pixelize].active?
          d << Metadata.display
        end.join(' ')
      )
    end

  private

    def file_size
      format(
        "%s MiB",
        Color.fs[@args[:target_file_size].fdiv(1024 * 8).round(2)]
      )
    end

    def video_bitrate
      format "vbr:%s", Color.vbr[@args[:video_bitrate].round]
    end

    def audio_bitrate
      format "abr:%s", Color.abr[$opt.audio_bitrate.value]
    end

    def video_dimensions
      format "dim:%s", Color.dim[@args[:video_dimensions]]
    end

    def duration
      format "time:%s", Color.dur[@args[:duration]]
    end

    def pixel_size
      format "pix:%s", Color.pix[@args[:pixelize].pixels_per_side]
    end
  end
end
