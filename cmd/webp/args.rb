# frozen_string_literal: true

module Webp
  class Args < DryRun
    def initialize(input_file, span, quality, video_filter, out_file)
      populate do |c|
        c.push(
          'ffmpeg', '-y', '-loglevel', 'error', '-nostdin',
          '-ss', span.start_position, '-i', input_file,
          '-t', span.speed_adjusted_seconds, *video_filter.build, '-c:v', 'libwebp',
          '-compression_level', '6', *quality, '-loop', '0', out_file
        )
      end
    end
  end
end
