# frozen_string_literal: true

require_relative 'webp_video_filter'
require_relative 'args'
require_relative 'cmd'
