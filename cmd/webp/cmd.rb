# frozen_string_literal: true
require 'forwardable'

module Webp
  class Info
    extend Forwardable
    def_delegators :@chunks, :<<
    def initialize = (@chunks = [])
    def display = (print @chunks.join(' ') unless $opt.dry_run)
  end

  class Cmd < Successor
    def initialize(input_files)
      @input_file = input_files.apply(Rule.new.limit_amount(1..1))
      @info = Info.new
    end

    def call
      preview if $opt.preview
      webp_args = Args.new(@input_file, span, quality, video_filter, out_file)
      @info.display
      Console.with_hidden_cursor(!$opt.dry_run) do
        webp_args.call { system(*webp_args) }.success? or begin out_file.unlink; exit 1 end
      end
      with_real_success { out_file.summary("WebP", show_metadata: false) }
    rescue Interrupt
      Console.clean_interrupt_chars
      out_file.unlink
      exit
    end

  private

    def preview
      FFplay::Cmd.new(
        input_file: @input_file,
        start_position: span.start_position,
        duration: span.speed_adjusted_seconds,
        video_filter: video_filter.build,
        mute: true,
      ).call
      exit
    end

    def quality = ['-quality', $opt.quality]

    def span
      @span ||=
        begin
          edges = Edges.new @input_file, @input_file.duration
          if $opt.blend.active? and $opt.blend.expand?
            edges.start = (edges.start - $opt.blend.duration.fdiv(2)).clamp(0..)
            edges.stop = (edges.stop + $opt.blend.duration.fdiv(2)).clamp(..@input_file.duration)
          end
          span = Span.new(edges).tap do
            @info << format('%ss', Color.dur[_1.seconds.round(1)])
          end
        end
    end

    def video_filter
      WebpVideoFilter.new(@input_file, span.seconds, crop, restrained_dimensions)
    end

    def restrained_dimensions
      @limited_width = [crop.width, @input_file.width].compact.min
      @limited_height = [crop.height, @input_file.height].compact.min
      if @limited_width <= @limited_height
        @limited_width = [@limited_width, $opt.short_side_limit, $opt.width].compact.min
        @limited_height = [@limited_height, $opt.long_side_limit, $opt.height].compact.min
      else
        @limited_width = [@limited_width, $opt.long_side_limit, $opt.width].compact.min
        @limited_height = [@limited_height, $opt.short_side_limit, $opt.height].compact.min
      end
      original_width, original_height = @input_file.width, @input_file.height
      if $opt.rotate&.size&.odd?
        @limited_width, @limited_height = @limited_height, @limited_width
        original_width, original_height = original_height, original_width
      end
      @restrained_dimensions ||= RestrainedDimensions.new(
        width: RestrainedWidth.new(
          original: original_width,
          explicit_limit: nil,
          implicit_limit: @limited_width,
          crop_limit: crop.width
        ),
        height: RestrainedHeight.new(
          original: original_height,
          explicit_limit: nil,
          implicit_limit: @limited_height,
          crop_limit: crop.height
        )
      ).tap { @info << Color.dim[_1.to_s] }
    end

    def crop
      @crop ||=
        case
        when $opt.crop.active?
          crop = ($opt.crop.value || PositionsStore.crop) or throw :halt, "Crop is unspecified"
          Crop.new(Area.from_xywh(crop))
        when $opt.detect_crop
          DetectCrop::Cmd.new(
            input_file: @input_file,
            ffprobe: @input_file.ffprobe,
            start_position: span.start_position.to_f,
            stop_position: span.stop_position.to_f,
          ).call
        else
          Struct.new(:width, :height, :x_offset, :y_offset).new
        end
    end

    def out_file
      @out_file ||=
        begin
          outname = $opt.outname || [
            @input_file.id,
            span.start_position.id,
            span.stop_position.id,
            "q#{$opt.quality}",
            @restrained_dimensions.to_s,
          ].tap do |n|
            n << "E#{$opt.speed.value.round(2)}" if $opt.speed.active?
            n << "b#{$opt.blend.method}:#{$opt.blend.duration}" if $opt.blend.active?
            n << "sharp" if $opt.sharpen
            n << "gray" if $opt.grayscale
            n << "norm" if $opt.normalize
          end
          OutFile.new(*[outname], ext: ".webp")
        end
    end
  end
end
