require_relative '../../lib/filter/video_filter'
require_relative '../../lib/video_dimensions/restrained_dimensions'

class WebpVideoFilter
  def initialize(input_file, duration, crop, restrained_dimensions)
    @input_file = input_file
    @duration = duration
    @crop = crop
    @restrained_dimensions = restrained_dimensions
  end

  def build
    VideoFilter.new do |vf|
      vf << FilterBlend.new(@duration).to_arg if $opt.blend.active?
      vf << $opt.delogo.to_arg if $opt.delogo.active? and not $opt.blend.active?
      vf << @crop.to_arg if @crop.is_a? Crop
      vf << rotate if $opt.rotate
      vf << scale if @restrained_dimensions.restrained?
      vf << $opt.speed.to_s if $opt.speed.active?
      vf << 'unsharp' if $opt.sharpen
      vf << normalize if $opt.normalize
      vf << 'format=gray' if $opt.grayscale
      vf << "minterpolate=fps=#{$opt.frame_rate || 25}" if $opt.speed.interpolate?
      vf << "fps=#{$opt.frame_rate}" if $opt.frame_rate and not $opt.speed.interpolate?
      vf.concat $opt.custom_vf
    end
  end

  def normalize = "normalize=smoothing=#{$opt.grayscale ? 5 : 10}"
  def rotate = $opt.rotate.map { "transpose=#{_1}" }.join(',')
  def scale = (@restrained_dimensions.to_scale + ",setsar=1:1")

  class FilterBlend
    # https://stackoverflow.com/questions/60043174/cross-fade-video-to-itself-with-ffmpeg-for-seamless-looping
    def initialize(duration)
      @bd = $opt.blend.duration
      @off = duration - @bd * 2
    end

    def to_arg = [beginning, ending, xfade].join(';')

  private

    def maybe_delogo = ($opt.delogo.to_arg if $opt.delogo.active?)
    def beginning = (["[0]trim=end=#@bd", *maybe_delogo].join(',') << "[begin]")
    def ending = (["[0]trim=start=#@bd", *maybe_delogo].join(',') << "[end]")
    def xfade = "[end][begin]xfade=#{$opt.blend.method}:duration=#{@bd}:offset=#@off"
  end
end
