# frozen_string_literal: true

module Silent
  class SingleFileCmd < Successor
    attr_writer :shared_data

    def initialize(input_file)
      @input_file = input_file
    end

    def call
      if stored_file_only?
        KeepingSingleFileCmd
      else
        ReplacingSingleFileCmd
      end.new(@input_file).call
    end

  private

    def stored_file_only?
      @shared_data
    end
  end
end
