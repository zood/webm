# frozen_string_literal: true

module Audio
  module Replace
    class Cmd < Successor
      def initialize(webm_file, opus_file)
        @webm_file = webm_file
        @opus_file = opus_file
      end

      def call
        replace_args = Args.new(@webm_file, @opus_file)
        replace_args.call { system(*replace_args) }.success? or exit 1

        with_real_success do
          return unless @webm_file.exists?

          replace_args.out_file.rename to: @webm_file.name
          @opus_file.unlink
          @webm_file.summary("Replace audio", show_metadata: false, indent: 4)
        end
      end
    end
  end
end
