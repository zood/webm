# frozen_string_literal: true

module Audio
  module Replace
    class Args < DryRun
      def initialize(webm_file, audio_file)
        @webm_file = webm_file
        @audio_file = audio_file

        populate do |c|
          c.push(
            *preamble, "-i", webm_file.name,
            "-i", audio_file.name, *copy_streams,
            out_file
          )
        end
      end

      def out_file
        WEBMFile.new(@webm_file.id, @audio_file.id)
      end

      private

      def preamble
        %w[ffmpeg -y -loglevel error -nostdin]
      end

      def copy_streams
        %w[-map 0:v -map 1:a -c:v copy].tap do |args|
          if video_container_allows_audio_codec?
            args.concat %w[-c:a copy]
          end
        end
      end

      def video_container_allows_audio_codec?
        Compatible.containers_for(audio_file_codec)
          .include?(@webm_file.container)
      end

      def audio_file_codec
        @audio_file.audio_codec
      end
    end
  end
end
