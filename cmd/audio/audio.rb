# frozen_string_literal: true

require_relative "evoke/evoke"
require_relative "replace/replace"

module Audio
  class Cmd
    def initialize(input_files)
      @input_file = input_files.apply(
        Rule.new.limit_amount(1..1)
      )
    end

    def call
      evoke
      replace
    end

    private

    def evoke
      @evoke ||= Evoke::Cmd.new(
        @input_file, span
      ).call
    end

    def replace
      return unless (webm = converted_webm).exists?
      return if webm == evoke.out_file
      Replace::Cmd.new(webm, evoke.out_file).call
    end

    def converted_webm
      WEBMFile.new(
        @input_file.id,
        span.start_position.id,
        span.stop_position.id
      )
    end

    def span
      @span ||= Span.new(
        Edges.new(
          @input_file,
          @input_file.duration
        )
      )
    end
  end
end
