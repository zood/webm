# frozen_string_literal: true

module Audio
  module Evoke
    class Args < DryRun
      include Streamable

      def initialize(input_file, span, out_file)
        @input_file = input_file
        @span = span

        populate do |c|
          c.push(
            *preamble, *start_position, "-i", @input_file,
            *seconds, *map_streams, "-vn", *$opt.audio_bitrate.to_a,
            "-ac", "2", *audio_filter, out_file
          )
        end
      end

      private

      def preamble
        %w[ffmpeg -y -loglevel error -nostdin]
      end

      def start_position
        return if $opt.span.start_at_beginning
        ["-ss", @span.start_position]
      end

      def seconds
        return if $opt.span.start_at_beginning
        ["-t", @span.seconds * $opt.speed.value]
      end

      def audio_filter
        AudioFilter.new do |af|
          af << "volume=#{$opt.volume}" if $opt.volume
          af << fade_audio if $opt.fade_audio
        end
      end

      def fade_audio
        $opt.fade_audio.duration(@span.seconds).to_arg
      end

      def map_streams
        return unless $opt.map_streams
        $opt.map_streams.split(",")
          .flat_map { |s| ["-map", zero_stream(s)] }
      end
    end
  end
end
