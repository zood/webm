# frozen_string_literal: true

require_relative "../../lib/metadata/metadata_parser"
require_relative "../../lib/role/sanitizeable"

class FileRanger
  using Sanitizeable
  using Metadata::Parser

  attr_reader :untitled, :good_files, :rename_pairs

  def initialize(input_files)
    @titled = Hash.new { |h, k| h[k] = [] }
    @untitled = []
    @good_files = []
    @rename_pairs = []
    arrange(input_files)
    decide
  end

private

  def arrange(input_files)
    input_files.each do |file|
      if (title = file.title)
        @titled[title.to_filename.sanitize] << file
      else
        @untitled << file
      end
    end
  end

  def decide
    @titled.each do |title, files|
      next process_single(files.last, title) if files.size == 1
      process_multiple(files, title)
      suffix.rewind
    end
  end

  def process_single(file, title)
    good_or_bad(
      old_name: file.basename.to_s,
      new_name: title + file.ext,
    )
  end

  def process_multiple(files, title)
    files.each do |file|
      good_or_bad(
        old_name: file.basename.to_s,
        new_name: title + suffix.next + file.ext,
      )
    end
  end

  def good_or_bad(old_name:, new_name:)
    if old_name == new_name
      @good_files << new_name
    else
      @rename_pairs << [old_name, new_name]
    end
  end

  def suffix
    @suffix ||= Enumerator.new do |e|
      e << value = ".a"
      loop { e << value = value.next }
    end
  end
end
