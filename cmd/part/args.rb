# frozen_string_literal: true

module Part
  class Args < DryRun
    def initialize(input_file, span, out_file)
      populate do |c|
        c.push(
          "ffmpeg", "-y", "-loglevel", "error", "-nostdin",
          "-ss", span.start_position, "-i", input_file,
          *_mute, "-t", span.seconds, *Metadata.to_arg,
          "-codec", "copy", *_copy_subtitles, out_file
        )
      end
    end

    def _copy_subtitles
      %w[-c:s copy]
    end

    def _mute
      %w[-map 0:v:0] if $opt.mute
    end
  end
end
