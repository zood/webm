# frozen_string_literal: true

module Part
  class Cmd < Successor
    def initialize(input_files)
      @input_file = input_files.apply(
        Rule.new.limit_amount(1..1)
      )
    end

    def call
      Metadata.video = @input_file.title
      Metadata.filepath = @input_file

      handle_surrounding_keyframes if $opt.keyframe.use_keyframe?

      part_args = Args.new(@input_file, span, out_file)
      part_args.call { system(*part_args) }.success? or exit 1
      with_real_success { out_file.summary("Part") }
    end

  private

    def span
      @span ||= Span.new(
        Edges.new @input_file, @input_file.duration
      )
    end

    def out_file
      @out_file ||= OutFile.new(
        "part",
        @input_file.id,
        span.start_position.id,
        ext: @input_file.ext,
      )
    end

    def handle_surrounding_keyframes
      $opt.span.start_position = nearly_keyframe_position
      @span = nil
    end

    def nearly_keyframe_position
       SurroundingKeyframes::Cmd.new(
        input_file: @input_file,
        start_position: span.start_position.to_f
       ).call($opt.keyframe.mode)
    end
  end
end
