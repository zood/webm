# frozen_string_literal: true

class ETA
  using NumericExtensions

  def initialize(delay:)
    @delay = delay
    @start = Time.now
    @ticks = -1
  end

  def tick(position)
    @ticks += 1
    return if @ticks < @delay or position.zero?

    rt = Time.now - @start
    Color.dark[
      "[ETA: %s]" % ((rt / position).round - rt).to_span
    ]
  end
end
