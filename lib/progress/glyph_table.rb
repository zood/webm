# frozen_string_literal: true

require_relative "bar_sequence"

class GlyphTable
  def initialize(glyphs, slots)
    @glyphs = glyphs
    @slots = slots
    @bar_sequence = BarSequence.new @glyphs
    @table = []
  end

  def table
    fill_in_exclusive_ranges
    fill_in_inclusive_range
    @table
  end

private

  def fill_in_exclusive_ranges
    (capacity - 1).times do |i|
      push (i * factor)...((i + 1) * factor)
    end
  end

  def fill_in_inclusive_range
    push ((capacity - 1) * factor)..(capacity * factor)
  end

  def push(range)
    @table << [range, @bar_sequence.next]
  end

  def capacity
    @glyphs.size * @slots
  end

  def factor
    1.fdiv capacity
  end
end
