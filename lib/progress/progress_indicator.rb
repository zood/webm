# frozen_string_literal: true

require_relative "progress_bar"
require_relative "../../extensions/numeric_extensions"
require_relative "eta"

class ProgressIndicator
  using NumericExtensions

  def initialize(glyphs:, color: ->_{_}, slots:, extra: ->{})
    @bar = ProgressBar.new(glyphs, slots)
    @color = color
    @extra = extra
    @eta = ETA.new(delay: 10)
  end

  def draw(position_factor)
    Console.updating_line do
      format("─┤%s├─ %s %s",
             @color.call(@bar.draw(position_factor)),
             @extra.call || position_factor.percent,
             @eta.tick(position_factor))
    end
  end
end
