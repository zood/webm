# frozen_string_literal: true

require "json"
require "pathname"
require_relative "../../extensions/string_extensions"

module PositionsStore
  extend self
  using StringExtensions

  STORE = Pathname "/tmp/mpv/positions.json"

  def method_missing(method)
    first_item[method.to_s]
  end

  def arrange(item)
    stack = self.empty? ? Array.new : self.stack[..7]
    stack.prepend(item)
    (self.json or Hash.new).tap do |d|
      d["stack"] = stack
      item["delogos"]&.tap { d["last_delogos"] = _1 }
    end
  end

  def save(item)
    STORE.parent.mkpath unless STORE.parent.exist?
    STORE.write arrange(item).to_json
  end

  def first_item = self.stack.first

  def pop_or_new(path)
    if idx = self.stack.find_index { _1["file"] == path.to_s }
      self.stack.delete_at(idx)
    else
      Hash.new
    end
  end

  def last_delogos
    last_delogos = json&.fetch("last_delogos")
    current_delogos = first_item["delogos"]
    if last_delogos
      cdelsize = current_delogos&.size.to_i
      ldelsize = last_delogos&.size.to_i
      if cdelsize < ldelsize
        $stderr.puts(Color.warn["Using #{"delogo".pluralize(ldelsize - cdelsize)} from before"])
      end
      return last_delogos
    end
  end

private

  def empty? = self.stack == [Hash.new]
  def stack = (self.json&.fetch("stack") || [Hash.new])
  def json = (@json ||= JSON.load(STORE.read) if STORE.file?)
end
