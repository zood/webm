# frozen_string_literal: true

module Console
  extend self

  def with_hidden_cursor(hide)
    hide_cursor if hide
    yield
    self
  ensure
    show_cursor if hide
  end

  def hide_cursor = print "\e[?25l"
  def show_cursor = print "\e[?25h"
  def clean_line = print "\r\e[K"
  def clean_interrupt_chars = print "\e\b\b"
  def updating_line = print "\r\e[K", yield
end
