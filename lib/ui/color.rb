# frozen_string_literal: true

# https://stackoverflow.com/questions/1489183/colorized-ruby-output
module Color
  extend self

  private def color(*color_codes)
    return ->str { str } unless $stdout.tty?
    ->str { "\e[#{color_codes.join(";")}m#{str}\e[0m" }
  end

  define_method(:err)  { color 91 }
  define_method(:succ) { color 7, 92 }
  define_method(:warn) { color 7, 33 }

  define_method(:abr) { color 94 }
  define_method(:dim) { color 95 }
  define_method(:dur) { color 96 }
  define_method(:fs)  { color 94 }
  define_method(:pix) { color 93 }
  define_method(:vbr) { color 93 }

  define_method(:implicit) { color 93 }
  define_method(:explicit) { color 92 }
  define_method(:inactive) { color 90 }

  define_method(:dark)   { color 2 }
  define_method(:casual) { color 7 }
  define_method(:danger) { color 1, 7, 31 }
  define_method(:head)   { color 7 }
  define_method(:meta)   { color 32 }
  define_method(:prompt) { color 1, 36 }
end
