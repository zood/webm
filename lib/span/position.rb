# frozen_string_literal: true

class Position
  include Comparable

  attr_reader :time

  def self.from_float(seconds)
    fail TypeError, "Nil position" if seconds.nil?
    if seconds.negative?
      fail ArgumentError, "Negative position: #{seconds}"
    end
    new Time.at(seconds).utc.round(6)
  end

  def initialize(time)
    fail TypeError, "#{time} is not a Time" unless time.is_a?(Time)
    @time = time
  end

  def to_s(shorten = false)
    return @time.strftime("%M:%S") if shorten
    to_string
  end

  def <=>(other)
    unless other.is_a? Position
      fail TypeError, "Can't compare Position with #{other.class}"
    end
    self.time <=> other.time
  end

  def -(other)
    unless other.is_a? Position
      fail TypeError, "Can't substruct #{other.class} from Position"
    end
    (self.time - other.time).round(6)
  end

  def to_f
    @time.to_f
  end

  def id
    format "%08.3f", @time
  end

private

  def to_string
    @time.strftime("%H:%M:%S.%6N")
  end

  @@pattern = /
  (?<hour>\d{2}):
  (?<min>\d{2}):
  (?<sec>\d{2})\.
  (?<millisec>\d{3})/x

  def self.to_seconds
    t = @@pattern.match(@string) or
      fail ArgumentError, format(
        "%s: %s",
        Color.err["Unsupported position format"],
        @string
      )

    (t["hour"].to_i * 3600) +
      (t["min"].to_i * 60) +
      (t["sec"].to_i) +
      (t["millisec"].to_i.fdiv(1000))
  end
end
