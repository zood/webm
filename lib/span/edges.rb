# frozen_string_literal: true

require_relative "../ui/positions_store"

class Edges
  attr_writer :start, :stop

  class Selector
    attr_reader :value

    def <<(condition, value)
      return if @value
      @value = value if condition
    end
  end

  attr_reader :entire_duration

  def initialize(input_file, entire_duration)
    @input_file = input_file
    @entire_duration = entire_duration
  end

  def start
    @start ||= Selector.new.tap do |s|
      s.<< $opt.span.start_position, $opt.span.start_position
      s.<< $opt.span.start_at_beginning, 0.0
      s.<< stored_positions_valid?, PositionsStore.start
    end.value
  end

  def stop
    @stop ||= Selector.new.tap do |s|
      s.<< $opt.span.stop_position, $opt.span.stop_position
      s.<< $opt.span.stop_at_end, entire_duration
      s.<< stored_positions_valid?, PositionsStore.stop
    end.value
  end

private

  def stored_positions_valid?
    @input_file.basename.to_s.eql?(
      PositionsStore.file&.then { |f| File.basename f }
    )
  end
end
