# frozen_string_literal: true

class Duration
  attr_reader :seconds

  def initialize(seconds)
    @seconds = Float(seconds)
    throw :halt, Color.err["Negative duration"] if seconds.negative?
  end

  def speed_adjusted_seconds
    seconds * $opt.speed.value
  end

  def to_s(shorten = false)
    Position.from_float(seconds).to_s(shorten)
  end
end
