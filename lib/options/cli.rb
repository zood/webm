# frozen_string_literal: true

require "optparse"
%w[audio_bitrate blend blur speed logo delogo sub crop keyframe].each { require_relative _1 }

module CLI
  @commands = Struct.new(*%i[amix audio convert frame info join mute part rename silent slice title voice webp]).new

  $opt = Struct.new(*%i[
    audio_bitrate audio_file back_volume blend blink_frame blur crop
    custom_vf cycle delogo detect_crop dry_run expand fade_audio fade_video
    filename_suffix frame_rate grayscale hardsub_external hardsub_internal
    height info_only keyframe logo long long_side_limit map_streams meta
    mute outname overlay normalize pixels_per_side preview quality real_run
    report_meta rotate sharpen short_side_limit span speed sub take_nth_frame
    target_file_size video_bitrate video_file volume width zoom
  ], keyword_init: true).new(
    audio_bitrate: AudioBitrate.new(64),
    blend: Blend.new(0.3),
    blur: Blur.new(20),
    crop: Crop.new,
    custom_vf: [],
    cycle: 1,
    delogo: Delogo.new,
    keyframe: Keyframe.new,
    logo: Logo.new,
    meta: Struct.new(*%i[edit from_filename set_audio title]).new(set_audio: true, title: ""),
    quality: '75',
    span: Struct.new(*%i[duration start_at_beginning start_position stop_at_end stop_position]).new,
    speed: Speed.new(1.0),
    sub: Sub.new
  )

  @commands.amix = OptionParser.new do |opts|
    opts.banner = "Usage: amix [options] [-a] audio-file [-v] video-file (any order)"
    opts.on("-B BITRATE", Integer, "Audio bitrate") { $opt.audio_bitrate.set _1 }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-V VOL", Float, "Foreground volume (factor, 1.0)") { $opt.volume = _1 }
    opts.on("--bv VOL", Float, "Background volume (factor, 0.1)") { $opt.back_volume = _1 }
    opts.separator "Positions"
    opts.on("-0", "Mix audio file from the start") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Mix audio file till the end") { $opt.span.stop_at_end = true }
    opts.on("-e", "Mix entire audio file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("--start SEC", Float, "Start position in seconds") { $opt.span.start_position = _1 }
    opts.on("--stop SEC", Float, "End position in seconds") { $opt.span.stop_position = _1 }
    opts.on("--duration SEC", Float, "Result duration") { $opt.span.duration = _1 }
    opts.separator "TITLE"
    opts.on("-t TITLE", "Set metadata title tag") { $opt.meta.title = _1 }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
    opts.separator "INPUT"
    opts.on("-a FILE", "Background audio file") { $opt.audio_file = _1 }
    opts.on("-v FILE", "Foreground file") { $opt.video_file = _1 }
  end

  @commands.audio = OptionParser.new do |opts|
    opts.on("-B BITRATE", Integer, "Audio bitrate") { $opt.audio_bitrate.set _1 }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-e", "Convert entire file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("-f [SEC|in=SEC:out=SEC]", "Fade audio in and out (def.: 0.5)") { $opt.fade_audio = FadeAudio.new(_1) }
    opts.on("-m STREAMS", "Map streams to output") { $opt.map_streams = _1 }
    opts.on("-V VOL", "Change volume (factor, 1.0)") { $opt.volume = _1 }
    opts.separator "Positions"
    opts.on("--start SEC", Float, "Start position in seconds") { $opt.span.start_position = _1 }
    opts.on("--stop SEC", Float, "End position in seconds") { $opt.span.stop_position = _1 }
    opts.on("--duration SEC", Float, "Result duration") { $opt.span.duration = _1 }
  end

  @commands.convert = OptionParser.new do |opts|
    opts.banner = "Usage: convert [options] file"
    opts.on("-E SPEED", Numeric, "Speed multiplier") { $opt.speed.set 1.fdiv(_1) }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-e", "Convert entire file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("-s SIZE", "Limit file's size (suffixes: k, m)") { $opt.target_file_size = _1 }
    opts.on("-V VOL", "Change volume (factor, 1.0)") { $opt.volume = _1 }
    opts.separator "Video mutation"
    opts.on("--vf GRAPH", "Append custom video filter") { $opt.custom_vf << _1 }
    opts.on("-r RATE", Integer, "Frame rate") { $opt.frame_rate = _1 }
    opts.on("-p [NUMBER]", Integer, "Pixelize video (def.: 42)") { $opt.pixels_per_side = _1 || 42; $opt.blur.drop }
    opts.on("--blur [STRENGTH]", Integer, "Blur video (def.: 20)") { $opt.blur.set _1; $opt.pixels_per_side = nil}
    opts.on("--overlay [POINT, X_SHIFT, Y_SHIFT]", "Overlay blured|pixelized video with intact area (crop is used)") { $opt.overlay = _1 || "0" }
    opts.on("--deinterlace", "Apply deinterlace filter") { $opt.custom_vf << "yadif" }
    opts.on("--cw", "Rotate video clockwise") { ($opt.rotate ||= []) << 1 }
    opts.on("--ccw ", "Rotate video counterclockwise") { ($opt.rotate ||= []) << 2 }
    opts.separator "Bitrate"
    opts.on("-B BITRATE", Integer, "Audio bitrate") { $opt.audio_bitrate.set _1 }
    opts.on("-b BITRATE", Integer, "Video bitrate") { $opt.video_bitrate = _1 }
    opts.separator "Crop"
    opts.on("-c [x,y,w,h]", "Crop the frame") { $opt.crop.set _1 }
    opts.on("-C [low|med|hi]", "Auto detect crop", %w[low med hi]) { $opt.detect_crop = _1&.to_sym || :med }
    opts.separator "Fade"
    opts.on("-f [SEC|in=SEC:out=SEC]", "Fade audio in and out (def.: 0.5)") { $opt.fade_audio = FadeAudio.new(_1) }
    opts.on("-F [SEC]", Float, "Fade video out into black (def.: 0.5)") { $opt.fade_video = _1 || 0.5 }
    opts.separator "Info"
    opts.on("-i", "Display summary and exit") { $opt.info_only = true }
    opts.on("-I", "Preview the result") { $opt.preview = true }
    opts.separator "Logo"
    opts.on("-l [x,y,w,h]", "Remove logo") { $opt.delogo << _1 }
    opts.on("--logo TEXT|IMAGE", "Draw logo") { $opt.logo.content = _1 }
    opts.on("--logo-alpha LVL", "Alpha-level of logo (0.0-1.0, def.: #{$opt.logo.alpha})") { $opt.logo.alpha = _1 }
    opts.on("--logo-color COLOR", "Logo font color (def.: #{$opt.logo.color})") { $opt.logo.color = _1 }
    opts.on("--logo-font NAME", "Logo font (def.: #{$opt.logo.font})") { $opt.logo.font = _1 }
    opts.on("--logo-padding PIX", "Logo padding (def.: #{$opt.logo.padding})") { $opt.logo.padding = _1 }
    opts.on("--logo-place POS", Integer, "Logo place (def.: #{$opt.logo.place})") { $opt.logo.place = _1 }
    opts.on("--logo-size NUM", "Logo font size (def.: #{$opt.logo.size})") { $opt.logo.size = _1 }
    opts.separator "Positions"
    opts.on("--start SEC", Float, "Start position in seconds") { $opt.span.start_position = _1 }
    opts.on("--stop SEC", Float, "End position in seconds") { $opt.span.stop_position = _1 }
    opts.on("--duration SEC", Float, "Result duration") { $opt.span.duration = _1 }
    opts.separator "Resize"
    opts.on("-W PIXELS", Integer, "Width") { $opt.width = _1 }
    opts.on("-H PIXELS", Integer, "Height") { $opt.height = _1 }
    opts.separator "Streams"
    opts.on("-m STREAMS", "Map streams to output") { $opt.map_streams = _1 }
    opts.on("-M", "Mute") { $opt.mute = true; $opt.audio_bitrate.set 0 }
    opts.separator "Subtitles"
    opts.on("-o STREAM", "Overlay with internal subtutles") { $opt.hardsub_internal = _1 }
    opts.on("-O SUBS", "Overlay with external subtutles") { $opt.hardsub_external = _1 }
    opts.on("--sub PRESET", %i[arial cambria cronos sans], "Subtitles preset (arial, cambria, cronos, sans)") { $opt.sub.preset_id = _1.to_sym }
    opts.on("--sub-alpha LVL", %w[1 2 3 4], "Alpha-level of subtitles (1-4)") { $opt.sub.alpha = _1.to_i }
    opts.on("--sub-bg RRGGBB", "Background color of subtitles") { $opt.sub.bg = _1 }
    opts.on("--sub-edit", "Edit subtitles") { $opt.sub.edit = _1 }
    opts.on("--sub-fg RRGGBB", "Foreground color of subtitles") { $opt.sub.fg = _1 }
    opts.on("--sub-outline NUM", Integer, %w[0 1 2 3 4], "Outline width in px (0-4)") { $opt.sub.outline = _1 }
    opts.on("--sub-shadow NUM", Integer, %w[0 1 2 3 4], "Depth of the shadow (0-4)") { $opt.sub.shadow = _1 }
    opts.on("--sub-size NUM", "Font size for subtitles (def.: 20)") { $opt.sub.size = _1 }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title") { $opt.meta.title = _1 }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
    opts.on("-N", "Use filename as the title") { $opt.meta.from_filename = true }
  end

  @commands.frame = OptionParser.new do |opts|
    opts.banner = "Usage: frame [options] image"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-M", "Mute") { $opt.mute = true }
    opts.on("-b", "Make blink frame") { $opt.blink_frame = true }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { $opt.meta.title = _1 }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
    opts.separator "RESIZE"
    opts.on("-W PIXELS", Integer, "Width") { $opt.width = _1 }
    opts.on("-H PIXELS", Integer, "Height") { $opt.height = _1 }
  end

  @commands.info = OptionParser.new

  @commands.join = OptionParser.new do |opts|
    opts.banner = "Usage: join [options] file1, file2..."
    opts.on("-c COUNT", Integer, "Cycle joined webm") { $opt.cycle = _1 }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { $opt.meta.title = _1 }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
  end

  @commands.mute = OptionParser.new do |opts|
    opts.banner = "Usage: mute [options] file"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { $opt.meta.title = _1 }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
  end

  @commands.part = OptionParser.new do |opts|
    opts.banner = "Usage: part [options] file"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-M", "Mute") { $opt.mute = true }
    opts.separator "Positions"
    opts.on("-0", "Start at file's beginning") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Stop at file's ending") { $opt.span.stop_at_end = true }
    opts.on("--pk", "Start parting at previous keyframe") { $opt.keyframe.mode = :preceeding }
    opts.on("--nk", "Start parting at next keyframe") { $opt.keyframe.mode = :succeeding }
    opts.on("--start SEC", Float, "Start position in seconds") { $opt.span.start_position = _1 }
    opts.on("--stop SEC", Float, "End position in seconds") { $opt.span.stop_position = _1 }
    opts.on("--duration SEC", Float, "Result duration") { $opt.span.duration = _1 }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { $opt.meta.title = _1 }
    opts.on("-N", "Use filename as the title") { $opt.meta.from_filename = true }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
  end

  @commands.rename = OptionParser.new do |opts|
    opts.banner = "Usage: rename [options] file"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-R", "Real run") { $opt.real_run = true }
  end

  @commands.silent = OptionParser.new do |opts|
    opts.banner = "Usage: silent [options] webm"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
  end

  @commands.slice = OptionParser.new do |opts|
    opts.banner = "Usage: slice [options] video"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-g", "Grayscale") { $opt.grayscale = true }
    opts.on("-n N", Integer, "Slice only every n-th frame (def.: 2") { $opt.take_nth_frame = _1}
    opts.on("--cw", "Rotate video clockwise") { ($opt.rotate ||= []) << 1 }
    opts.on("--ccw", "Rotate video counterclockwise") { ($opt.rotate ||= []) << 2 }
    opts.on("--deinterlace", "Apply deinterlace filter") { $opt.custom_vf << "yadif" }
    opts.separator "Positions"
    opts.on("-0", "Start at file's beginning") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Stop at file's ending") { $opt.span.stop_at_end = true }
    opts.on("-e", "Slice entire video") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("--start SEC", Float, "Start position in seconds") { $opt.span.start_position = _1 }
    opts.on("--stop SEC", Float, "End position in seconds") { $opt.span.stop_position = _1 }
    opts.on("--duration SEC", Float, "Result duration") { $opt.span.duration = _1 }
    opts.separator "Crop"
    opts.on("-c [x,y,w,h]", "Crop the frame") { $opt.crop.set _1 }
    opts.on("-C [low|med|hi]", "Auto detect crop", %w[low med hi]) { $opt.detect_crop = _1&.to_sym || :med }
    opts.on("-l [x,y,w,h]", "Remove logo") { $opt.delogo << _1 }
    opts.separator "Resize"
    opts.on("-S PIXELS", Integer, "Short side limit") { $opt.short_side_limit = _1 }
    opts.on("-L PIXELS", Integer, "Long side limit") { $opt.long_side_limit = _1 }
    opts.on("-W PIXELS", Integer, "Width limit") { $opt.width = _1 }
    opts.on("-H PIXELS", Integer, "Height limit") { $opt.height = _1 }
  end

  @commands.title = OptionParser.new do |opts|
    opts.banner = "Usage: title [options] file(s)"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-N", "Use filename as the title") { $opt.meta.from_filename = true }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
    opts.on("-e", "Edit tiles with editor one by one") { $opt.meta.edit = :single }
    opts.on("-E", "Edit tile with editor in bunch") { $opt.meta.edit = :bunch }
    opts.on("-t TITLE", "Set metadata title tag") { $opt.meta.title = _1 }
  end

  @commands.voice = OptionParser.new do |opts|
    opts.banner = "Usage: voice [options] [-a] audio-file [-v] video-file (any order)"
    opts.on("-B BITRATE", Integer, "Audio bitrate") { $opt.audio_bitrate.set _1 }
    opts.on("-V VOL", "Change volume (factor, 1.0)") { $opt.volume = _1 }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-f [SEC|[in=SEC:][out=SEC]]", "Fade audio in and out (def.: 0.5)") { $opt.fade_audio = FadeAudio.new(_1) }
    opts.on("-l", "Last untill the longest stream ends") { $opt.long = true }
    opts.on("-o OUTNAME", "Output file name (with no .ext)") { $opt.outname = _1 }
    opts.on("-z POINT", Integer, "Zoom to one of points") { $opt.zoom = _1 }
    opts.separator "Positions"
    opts.on("-0", "Mix audio file from the start") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Mix audio file till the end") { $opt.span.stop_at_end = true }
    opts.on("-e", "Mix entire audio file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("--start SEC", Float, "Start position in seconds") { $opt.span.start_position = _1 }
    opts.on("--stop SEC", Float, "End position in seconds") { $opt.span.stop_position = _1 }
    opts.on("--duration SEC", Float, "Result duration") { $opt.span.duration = _1 }
    opts.separator "TITLE"
    opts.on("-t TITLE", "Set metadata title tag") { $opt.meta.title = _1 }
    opts.on("-n", "Don't include audio metadata") { $opt.meta.set_audio = false }
    opts.on("-T", "Insert metadata title") { $opt.meta.title = nil }
    opts.separator "INPUT"
    opts.on("-a FILE", "Audio file") { $opt.audio_file = _1 }
    opts.on("-v FILE", "Video file") { $opt.video_file = _1 }
    opts.separator "RESIZE"
    opts.on("-W PIXELS", Integer, "Width") { $opt.width = _1 }
    opts.on("-H PIXELS", Integer, "Height") { $opt.height = _1 }
  end

  @commands.webp = OptionParser.new do |opts|
    opts.banner = "Usage: webp [options] video"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-g", "Grayscale") { $opt.grayscale = true }
    opts.on("-o OUTNAME", "Output file name (with no .ext)") { $opt.outname = _1 }
    opts.on("-q QUALITY", "Quality of the output WebP") { $opt.quality = _1 }
    opts.on("-E SPEED", Numeric, "Speed multiplier") { $opt.speed.set 1.fdiv(_1) }
    opts.on("-b METHOD[:SECONDS]", "Blend ending into the beginning
            (methods: fade, left, right, down, up, bl, br, tl, tr)") { $opt.blend.set _1 }
    opts.on("-x", "Expand span time by blending duration") { $opt.blend.expand = true }
    opts.on("--sharpen", "Sharpen the image") { $opt.sharpen = true }
    opts.on("--normalize", "Normalize the image") { $opt.normalize = true }
    opts.on("--cw", "Rotate video clockwise") { ($opt.rotate ||= []) << 1 }
    opts.on("--ccw", "Rotate video counterclockwise") { ($opt.rotate ||= []) << 2 }
    opts.on("--deinterlace", "Apply deinterlace filter") { $opt.custom_vf << "yadif" }
    opts.on("--vf GRAPH", "Append custom video filter") { $opt.custom_vf << _1 }
    opts.on("-r FPS", "Frames per second") { $opt.frame_rate = _1 }
    opts.on("-I", "Preview the result") { $opt.preview = true }
    opts.separator "Positions"
    opts.on("-0", "Start at file's beginning") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Stop at file's ending") { $opt.span.stop_at_end = true }
    opts.on("-e", "Use entire file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("--start SEC", Float, "Start position in seconds") { $opt.span.start_position = _1 }
    opts.on("--stop SEC", Float, "End position in seconds") { $opt.span.stop_position = _1 }
    opts.on("--duration SEC", Float, "Result duration") { $opt.span.duration = _1 }
    opts.separator "Crop"
    opts.on("-c [x,y,w,h]", "Crop the frame") { $opt.crop.set _1 }
    opts.on("-C [low|med|hi]", "Auto detect crop", %w[low med hi]) { $opt.detect_crop = _1&.to_sym || :med }
    opts.on("-l [x,y,w,h]", "Remove logo") { $opt.delogo << _1 }
    opts.separator "Resize"
    opts.on("-S PIXELS", Integer, "Short side limit") { $opt.short_side_limit = _1 }
    opts.on("-L PIXELS", Integer, "Long side limit") { $opt.long_side_limit = _1 }
    opts.on("-W PIXELS", Integer, "Width limit") { $opt.width = _1 }
    opts.on("-H PIXELS", Integer, "Height limit") { $opt.height = _1 }
end

  def self.commands_description
    %{Commands are:
    amix    > Merge external audio into existing one
    audio   > Evoke [and replace] audiotrack
    convert > Convert to webm
    frame   > Make one-frame webm out of image
    info    > Display a brief info about the media
    join    > Concatenate several webms into one
    mute    > Remove the audio track
    part    > Extract part of file
    rename  > Rename the file according to its metadata
    silent  > Silence the whole media or a region
    slice   > Extract every frame of the video
    title   > Set metadata title tag to the webm
    voice   > Make a webm, merging a video file and an audio
    webp    > Make an animated WebP image

    See "webm COMMAND -h" for more information on a specific command}
  end

  def self.parse_global_options
    parser = OptionParser.new do |opts|
      opts.banner = "Usage: webm COMMAND [options]"
      opts.separator ""
      opts.separator "Global options:"
      opts.on("--suf SUFFIX", "Append suffix to filename") { $opt.filename_suffix = _1 }
      opts.separator commands_description
    end
    parser.order!
  rescue OptionParser::ParseError => e
    puts Color.err[e.message]
    throw :halt, parser.help
  end

  def self.parse_command
    @command = guess_command
    parser = @commands[@command]
    parser.parse!
  rescue OptionParser::ParseError => e
    puts Color.err[e.message]
    puts parser.help if parser
    exit 1
  end

  def self.guess_command
    command = ARGV.shift or
      fail(OptionParser::MissingArgument,
           Color.err["Provide a command:\n\n"] + commands_description)

    guesses = @commands.members.select { |c| c.match?(/^#{command}/) }

    case guesses.size
    when 0
      fail OptionParser::InvalidArgument, Color.err["Unknown command -> #{command}"]
    when 1
      guesses.first
    else
      fail OptionParser::AmbiguousArgument,
        Color.err["Umbiguous command -> "] +
        "#{command}, matched: [#{guesses.join(", ")}]"
    end
  end

  def self.parse
    parse_global_options
    parse_command
    @command
  end
end
