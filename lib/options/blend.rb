# frozen_string_literal: true

module CLI
  class Blend
    attr_reader :duration, :method
    attr_writer :expand

    def initialize(duration)
      @duration = duration
      @active = false
      @expand = false
    end

    def set(spec)
      @active = true
      method = spec[/[a-z]+/]
      abort 'No blending method provided' unless method
      @method = %w[
        fade
        smoothleft smoothright smoothdown smoothup
        diagbl diagbr diagtl diagtr
      ].find { _1.include? method }
      abort "Unsupported blending method: #{method}" unless @method
      spec[/\d+(:?\.\d+)?/]&.tap { @duration = _1.to_f }
    end

    def active? = @active
    def expand? = @expand
  end
end
