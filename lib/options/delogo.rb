# frozen_string_literal: true

require_relative "../ui/positions_store"

module CLI
  class Delogo
    PATTERN = /\d{1,4},\d{1,4},\d{1,4},\d{1,4}/

    def initialize
      @specifiers = []
    end

    def active? = !@specifiers.empty?

    def <<(specifier)
      if specifier
        validate specifier
      else
        specifier = PositionsStore.last_delogos&.shift || throw(
          :halt, format(
            "%s",
            Color.err["Delogo specifier is not provided"]
          ))
      end
      @specifiers << specifier
    end

    def to_arg
      @specifiers.map do |specifier|
        format(
          "delogo=%s%s:show=0",
          specifier.tr(",", ":"),
          preview
        )
      end
    end

  private

    def preview
      $opt.preview ? ":show=1" : ""
    end

    def validate(specifier)
      specifier or throw :halt, Color.err["No delogo specifier provided"]

      throw :halt, format(
        "%s: %s",
        Color.err["Invalid delogo specifier"],
        specifier
      ) unless specifier.match?(PATTERN)
    end
  end
end
