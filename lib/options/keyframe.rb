# frozen_string_literal: true

module CLI
  Keyframe = Struct.new(*%i[mode]) do
    def use_keyframe?
      !!mode
    end
  end
end
