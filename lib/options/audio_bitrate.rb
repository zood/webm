# frozen_string_literal: true

require_relative "option"

module CLI
  class AudioBitrate < Option
    def boring?
      passive? or value.zero?
    end

    def to_a
      return if value.zero?
      %W[-b:a #{value}k]
    end

    def to_s
      to_a&.join " "
    end
  end
end
