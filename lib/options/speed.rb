# frozen_string_literal: true

require_relative "option"

module CLI
  class Speed < Option
    def to_s = "setpts=#{value}*PTS"

    def interpolate?
      self.value > 1.fdiv(Rational('2/3'))
    end
  end
end
