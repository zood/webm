# frozen_string_literal: true

require "forwardable"

class DryRun
  extend Forwardable

  attr_reader :output, :status, :out_file

  def_delegators :arguments, :<<, :push, :compact!, :concat

  def call(force: false, &block)
    return dry_run(force, &block) if $opt.dry_run
    @output = yield
    @status = $?&.exitstatus
    self
  end

  def success?
    return true if $opt.dry_run
    @status ||= nil
    @status&.zero?
  end

  def arguments
    @arguments ||= []
  end

  alias_method :to_a, :arguments

private

  def populate
    yield self
    arguments.compact!
    arguments.map!(&:to_s)
  end

  def dry_run(force, &block)
    case $opt.dry_run
    when 1 then puts lvl1
    when 2 then puts lvl2
    end
    forced_call(&block) if force
    self
  end

  def lvl1
    [Color.head[self.class.name],
     to_a
      .slice_before(&parameter)
      .map { |i| i.join(' ') }
    ]
  end

  def lvl2
    to_a
      .slice_before(&parameter)
      .map { |chunk| chunk.map(&shell_safe) }
      .map { |i| i.join(' ') }
      .join(" \\\n")
  end

  def parameter
    proc { |i| i.match?(/-[a-z]/) }
  end

  def shell_safe
    proc { |i| i.match?(/[ \[]/) ? %["#{i}"] : i }
  end

  def forced_call
    @output = yield
    @status = $?&.exitstatus
  end
end
