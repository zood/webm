# frozen_string_literal: true

require_relative "out_file"

class MOVFile < OutFile
  def initialize(*name_chunks)
    super(*name_chunks, ext: ".mov")
  end
end
