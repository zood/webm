# frozen_string_literal: true

require_relative "out_file"

class WMVFile < OutFile
  def initialize(*name_chunks)
    super(*name_chunks, ext: ".wmv")
  end
end
