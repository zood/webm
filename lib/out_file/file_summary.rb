# frozen_string_literal: true

module FileSummary
  def self.string(
    indent: 0,
    message: nil,
    filename:,
    filesize:,
    metadata:,
    show_metadata: true
  )
    Array.new.tap do |s|
      s << ' ' * indent
      s << message + ':' if message
      s << filename
      s << filesize
      s << metadata if metadata && show_metadata
    end.join(' ')
  end
end
