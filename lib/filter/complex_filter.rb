# frozen_string_literal: true

require_relative "filter"

module ComplexFilter
  class Task
    attr_reader :filters, :head, :tail

    def initialize(head, tail, nomap)
      @head = head
      @tail = tail
      @nomap = nomap
      @filters = []
    end

    def <<(filtergraph)
      @filters << filtergraph
    end

    def to_arg
      format(
        "%s%s%s",
        wrap(head),
        filters.compact.join(","),
        wrap(tail)
      )
    end

    def map
      return [] if @nomap
      ["-map", effective_tail]
    end

    def void?
      filters.none?
    end

  private

    def effective_tail
      void? ? head : "[#{tail}]"
    end

    def wrap(markers)
      case markers
      when String then "[%s]" % markers
      when Array then
        markers.map { |m| "[#{m}]" }.join
      end
    end
  end

  class Build
    PREFIX = "-filter_complex"

    attr_reader :tasks

    def initialize
      @tasks = []
      yield self if block_given?
    end

    def add_task(id, head, tail, nomap: false)
      task = Task.new(head, tail, nomap)
      @tasks << task
      yield task
    end

    def add_map(marker)
      add_task(:single_map, marker, nil) {}
    end

    def to_a
      [*args, *maps]
    end

  private

    def args
      es = effective_tasks.map(&:to_arg)
      return [] if es.none?
      [PREFIX, es.join(";")]
    end

    def maps
      tasks.flat_map(&:map)
    end

    def effective_tasks
      tasks.reject(&:void?)
    end
  end
end
