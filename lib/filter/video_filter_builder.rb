# frozen_string_literal: true

require_relative "../role/streamable"

class VideoFilterBuilder
  include Streamable

  def initialize(**args)
    @input_file = args[:input_file]
    @span = args[:span]
    @crop = args[:crop]
    @pixelize = args[:pixelize]
    @restrained_dimensions = args[:restrained_dimensions]
    @overlay_manager = args[:overlay_manager]
    @sharpened = !!args[:do_not_sharpen]
  end

  def call
    VideoFilter.new do |vf|
      vf << overlay if $opt.overlay
      vf.concat $opt.delogo.to_arg
      if @crop.is_a?(Crop)
        vf << @crop.to_arg
        vf << sharpen_once
      end
      vf << fade_video if $opt.fade_video
      vf << rotate if $opt.rotate
      if @restrained_dimensions.restrained?
        vf << scale
        vf << sharpen_once
      end
      vf << hardsub_internal if $opt.hardsub_internal
      vf << hardsub_external if $opt.hardsub_external
      vf << $opt.logo.to_arg if $opt.logo.content
      vf << $opt.speed.to_s if $opt.speed.active? and not $opt.blend.active?
      vf << @pixelize.to_s if @pixelize.active? and not $opt.overlay
      vf << blur if $opt.blur.active? and not $opt.overlay
      vf.concat $opt.custom_vf
      vf << minterpolate if $opt.speed.interpolate?
    end
  end

private

  def minterpolate = "minterpolate=fps=25"
  def blur = "boxblur=#{$opt.blur}"

  def overlay
    mode = case
           when @pixelize.active? then @pixelize
           when $opt.blur then blur
           else blur
           end
    crop = Crop.new @overlay_manager.fg_area
    format(
      "[0:v]#{mode}[bg];[0:v]%s[fg];[bg][fg]overlay=%s",
      crop.to_arg,
      @overlay_manager.area_overlay.to_s
    )
  end

  def rotate
    $opt.rotate.map { "transpose=#{_1}" }.join(',')
  end

  def fade_video
    format(
      "fade=out:st=%s:d=%s",
      @span.seconds - $opt.fade_video,
      $opt.fade_video
    )
  end

  def scale
    @restrained_dimensions.to_scale + ",setsar=1:1"
  end

  def sharpen_once
    return if @sharpened
    @sharpened = true
    "unsharp"
  end

  def hardsub_internal
    return if $opt.hardsub_external

    extractor = Subtitles::Extract::Internal::Cmd.new(
      container: @input_file,
      stream_id: subtitle_stream($opt.hardsub_internal),
      start_position: @span.start_position,
      seconds: @span.seconds,
    )

    Subtitles::String.from(extractor.call)
  end

  def hardsub_external
    extractor = Subtitles::Extract::External::Cmd.new(
      subs_file: $opt.hardsub_external,
      start_position: @span.start_position,
      seconds: @span.seconds,
    )

    Subtitles::String.from(extractor.call)
  end
end
