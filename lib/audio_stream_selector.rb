# frozen_string_literal: true

module AudioStreamSelector
  extend self

  def call(ffprobe)
    @ffprobe = ffprobe
    select
  end

  def select
    return if $opt.map_streams or $opt.mute or
      not @ffprobe.multiple_audio_streams?
    interact
  end

  def interact
    warn
    $opt.map_streams = "0,#{poll}"
  end

  def warn
    puts Color.prompt["Found audio streams:"]
    @ffprobe.audio_streams.each.with_index(1) { |s, i| puts "#{i}: #{s}" }
    puts Color.prompt["Which one to use?"]
  end

  def poll
    while (index = STDIN.gets.to_i) > @ffprobe.audio_streams_count
      printf(
        "%s %s ",
        Color.warn["Wrong stream index, should be under:"],
        @ffprobe.audio_streams_count
      )
    end
    index
  rescue Interrupt
    exit
  end
end
