# frozen_string_literal: true

require_relative "cropped_dimensions"

class RotatedCroppedDimensions < CroppedDimensions
  def initialize(**)
    super
    apply_rotation
  end

private

  def apply_rotation
    return unless $opt.rotate
    @original_width, @original_height =
      @original_height, @original_width
  end

end
