# frozen_string_literal: true

require_relative "restrained_width"
require_relative "restrained_height"

class RestrainedDimensions
  attr_reader :width, :height

  def initialize(width:, height:)
    @width = width
    @height = height
  end

  def restrained?
    [@width, @height].any?(&:restrained?)
  end

  def restrained_width
    @width.value if @width.restrained?
  end

  def restrained_height
    @height.value if @height.restrained?
  end

  def maybe_scaled
    case [width.implicitly_restrained?, height.implicitly_restrained?]
    when [false, false] then [width.value, height.value]
    when [true, false]  then [width.value, (height.value * width.scale_factor).round]
    when [false, true]  then [(width.value * height.scale_factor).round, height.value]
    when [true, true] then abort("OOPS!")
    end
  end

  def to_s = maybe_scaled.join('x')

  def swap
    @width, @height = @height, @width
  end

  def to_scale
    case [@width.explicitly_restrained?, @height.explicitly_restrained?]
    when [true, true] then format("scale=%s:%s", restrained_width, restrained_height)
    when [true, false] then format("scale=%s:-1", restrained_width)
    when [false, true] then format("scale=-1:%s", restrained_height)
    when [false, false]
      case [@width.implicitly_restrained?, @height.implicitly_restrained?]
      when [true, true] then format("scale=%s:-1", restrained_width)
      when [true, false] then format("scale=%s:-1", restrained_width)
      when [false, true] then format("scale=-1:%s", restrained_height)
      when [false, false] then ""
      end
    end
  end
end
