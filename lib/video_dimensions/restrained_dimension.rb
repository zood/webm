# frozen_string_literal: true

class RestrainedDimension
  attr_reader :original_value, :explicit_limit

  def initialize(**args)
    @original_value = args[:original]
    @explicit_limit = args[:explicit_limit]
    @implicit_limit = args[:implicit_limit]
    @crop_limit = args[:crop_limit]
  end

  def value
    @explicit_limit or limit(
      implicitly_limited(@original_value),
      crop_limited(@original_value)
    )
  end

  def restrained?
    explicitly_restrained? or implicitly_restrained?
  end

  def explicitly_restrained?
    !!(@explicit_limit and
      @explicit_limit != @original_value)
  end

  def implicitly_restrained?
      implicitly_limited(@original_value).<(crop_limited(@original_value))
  end

  def scale_factor
    value.fdiv crop_limited(@original_value)
  end

private

  def implicitly_limited(value)
    limit @implicit_limit, value
  end

  def crop_limited(value)
    limit @crop_limit, value
  end

  def limit(value, limit)
    [limit, value].compact.min
  end
end
