# frozen_string_literal: true

class Area
  include Comparable

  attr_reader :width, :height, :x_offset, :y_offset

  def self.from_xywh(specifier)
    x, y, w, h = specifier.split(/\D/).map(&method(:Integer))
    self.new x: x, y: y, w: w, h: h
  end

  def self.from_whxy(specifier)
    w, h, x, y = specifier.split(/\D/).map(&method(:Integer))
    self.new x: x, y: y, w: w, h: h
  end

  def initialize(x: 0, y: 0, w:, h:)
    @x_offset, @y_offset, @width, @height = x, y, w, h
  end

  def <=>(other)
    width * height <=> other.width * other.height
  end

  def to_whxy_spec
    with_defined_attributes do
      [width, height, x_offset, y_offset].join(':')
    end
  end

  alias_method :to_s, :to_whxy_spec

  def to_xywh_spec
    with_defined_attributes do
      [x_offset, y_offset, width, height].join(':')
    end
  end

private

  def with_defined_attributes
    return unless [x_offset, y_offset, width, height].all?
    yield
  end
end
