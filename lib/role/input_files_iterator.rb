# frozen_string_literal: true

require_relative "../../extensions/array_extensions"

module InputFilesIterator
  using ArrayExtensions

  def call
    @input_files.each_with_reverse_index do |input_file, index|
      report_progress index
      single_file_command.new(input_file).call
    end
  end

private

  def report_progress(index)
    if @input_files.size > 5
      print "#{index}: "
    end
  end

  def single_file_command
    Object.const_get "#{command}::SingleFileCmd"
  end

  def command
    self.class.name.split("::").first
  end
end
