# frozen_string_literal: true

require_relative "input_files_iterator"

module SharedInputFilesIterator
  include InputFilesIterator

  def call
    @input_files.each.with_index(1) do |input_file, index|
      report_progress index

      cmd = single_file_command.new(input_file)
      cmd.shared_data = shared_data
      cmd.call
    end
  end

private

  def shared_data
    raise NotImplementedError,
      "#{self.class} should implement #{__callee__}"
  end
end
