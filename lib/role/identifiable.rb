# frozen_string_literal: true

require "digest/sha1"

module Identifiable
  def id(length = 4)
    Digest::SHA1.hexdigest(File.basename path)[0, length]
  end

  def path
    raise NotImplementedError,
      "#{self.class} should implement #{__callee__}"
  end

  refine String do
    def id(length = 4)
      Digest::SHA1.hexdigest(self)[0, length]
    end
  end
end
