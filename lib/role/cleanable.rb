# frozen_string_literal: true

module Cleanable
  refine String do
    def clean
      return self if self.empty?
      delete_prefix("file:")
    end
  end
end
