# frozen_string_literal: true

module ColorConverter
  def rgb_to_bgr(rgb)
    # https://ffmpeg.org/ffmpeg-utils.html#Color
    rgb.delete_prefix("#").chars.each_slice(2).to_a.reverse.join
  end
end
