# frozen_string_literal: true

require "pathname"

module FFprobe
  class Build
    include Audible
    include Visible
    include Formatable
    include Containable

    attr_reader :path, :file_arg

    def initialize(path)
      @path = Pathname.new path

      streams = Streams.new(json[:streams])
      @audio_streams = streams.audio.map(&AudioStream.method(:new))
      @video_streams = streams.video.map(&VideoStream.method(:new))
    end

  private

    def fformat
      @fformat ||= Format.new(json[:format])
    end

    def json
      @json ||= Reader.json @path
    end

    def try_find(property, streams)
      streams.find(&property)&.public_send(property)
    end
  end
end
