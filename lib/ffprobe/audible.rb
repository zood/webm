# frozen_string_literal: true

require_relative "../../extensions/array_extensions"

module Audible
  using ArrayExtensions

  attr_reader :audio_streams

  def audio_codec
    try_find :codec, audio_streams
  end

  def audible?
    audio_streams.any?
  end

  def mute?
    audio_streams.empty?
  end

  def audio_streams_count
    audio_streams.count
  end

  def multiple_audio_streams?
    audio_streams_count > 1
  end

  def hi_res_audio?
    audio_streams.single.bit_depth > 16
  end

  def title
    audio_streams.single.title
  end
end
