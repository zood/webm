# frozen_string_literal: true

module Visible
  def crop
    try_find :crop, @video_streams
  end

  def framerate
    try_find :framerate, @video_streams
  end

  def height
    try_find :height, @video_streams
  end

  def width
    try_find :width, @video_streams
  end

  def is_picture?
    @video_streams.any?(&:is_picture?)
  end

  def video_codec
    try_find :codec, @video_streams
  end

  def visual?
    @video_streams.any?
  end

  def vp9?
    @video_streams.any?(&:vp9?)
  end
end
