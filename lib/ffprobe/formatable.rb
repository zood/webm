# frozen_string_literal: true

module Formatable
  def audio_meta
    fformat.tags
      &.values_at(:artist, :title, :album)
      &.compact
      &.then { |tags| tags.size < 3 ? nil : tags }
      &.then { |tags| format "%s • %s • %s", *tags }
  end

  def duration
    fformat.duration
  end

  def title
    fformat.title
  end
end
