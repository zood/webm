# frozen_string_literal: true

module FFprobe
  class Streams
    attr_reader :streams

    def initialize(streams)
      @streams = streams
    end

    def audio
      streams_of_type "audio"
    end

    def video
      streams_of_type "video"
    end

  private

    def streams_of_type(type)
      streams.select { |s| s[:codec_type] == type }
    end
  end
end
