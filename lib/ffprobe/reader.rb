# frozen_string_literal: true

require "json"

module FFprobe
  module Reader
    extend self

    def json(path)
      wrong_file "File not exists", path unless path.exist?
      JSON.parse(exec_ffprobe(path), symbolize_names: true)
    end

  private

    def exec_ffprobe(path)
      output = IO.popen(
        %W[ffprobe
           -i file:#{path}
           -v quiet
           -print_format json=compact=1
           -show_format
           -show_streams],
           &:read
      )

      wrong_file("Invalid file", path) unless $?.success?
      output
    end

    def wrong_file(msg, file)
      throw :halt, format("%s: %s", Color.err[msg], file)
    end
  end
end
