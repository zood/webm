# frozen_string_literal: true

require_relative "stream"

module FFprobe
  class AudioStream < Stream
    def bit_depth
      stream[:bits_per_raw_sample].to_i
    end

    def to_s
      tags[:language]
    end
  end
end
