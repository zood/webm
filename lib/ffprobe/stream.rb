# frozen_string_literal: true

module FFprobe
  class Stream
    attr_reader :stream

    def initialize(stream)
      @stream = stream
    end

    def codec
      stream&.dig :codec_name
    end

    def tags
      stream[:tags] or {}
    end
  end
end
