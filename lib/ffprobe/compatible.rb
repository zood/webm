# frozen_string_literal: true

module Compatible
  extend self

    TABLE = {
      webm: %i[opus vorbis mjpeg png vp8 vp9],
      mp4: %i[aac h264],
    }

  def containers_for(codec)
    TABLE.select { |_,v| v.include?(codec.to_sym) }.keys
  end

  def codecs_for(container)
    TABLE[container.to_sym]
  end
end
