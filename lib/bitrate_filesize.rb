# frozen_string_literal: true

class BitrateFilesize
  def initialize(**args)
    @video_dimensions = args[:video_dimensions]
    @seconds = args[:seconds]
    @audio_bitrate = args[:audio_bitrate]
    @bitrate_factor = args[:bitrate_factor]
    set_default
  end

  def video_bitrate
    return $opt.video_bitrate if $opt.video_bitrate
    (target_file_size_kbites - @seconds * @audio_bitrate) / @seconds
  end

  def target_file_size_kbites
    if (tfs = $opt.target_file_size)
      kbites = case tfs[-1]
               when /k/i then tfs.to_f * 8
               when /m/i then tfs.to_f * 8 * 1024
               else tfs.to_f * 8 / 1024
               end
      return kbites
    end

    ($opt.video_bitrate + @audio_bitrate) * @seconds
  end

private

  def set_default
    if [$opt.video_bitrate, $opt.target_file_size].none?
      $opt.video_bitrate =
        (@video_dimensions.assumed_bitrate /
         @bitrate_factor * $opt.speed.value
        ).to_i
    end
  end
end
