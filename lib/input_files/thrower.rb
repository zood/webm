# frozen_string_literal: true

module InputFiles
  module Thrower

  private

    def throw_amount_mismatch(expected, actual)
      throw :halt, format(
        "%s %d for %s",
        Color.err["Wrong input files quantity:"],
        actual,
        expected
      )
    end

    def throw_absent_mandatory_type(shortage)
      throw :halt, format(
        "%s: %s",
        Color.err["No files of mandatory type"],
        shortage.join(", ")
      )
    end

    def throw_boundary_exceeding(type, expected, actual)
      throw :halt, format(
        "%s %d for %s",
        Color.err["Type '#{type}' is out of boundaries:"],
        actual,
        expected
      )
    end

    def throw_unexpected_type(type)
      throw :halt, Color.err["Provided unexpected type: #{type}"]
    end
  end
end
