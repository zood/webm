# frozen_string_literal: true

module InputFiles
  module Classifier
    extend self

    def by_extension(files)
      Hash.new(0).tap do |extensions|
        files.each do |f|
          extensions[symbolize_extension f] += 1
        end
      end
    end

    def symbolize_extension(filename)
      File.extname(filename)
        .delete_prefix(".")
        .downcase
        .to_sym
    end
  end
end
