# frozen_string_literal: true

require_relative "../role/identifiable"
require_relative "../role/human_readable"
require_relative "../role/colorizeable"

class InputFile
  include Identifiable
  using Colorizeable
  using HumanReadable

  attr_reader :path

  def initialize(path)
    @path = Pathname.new path
  end

  def method_missing(m, *args, &block)
    ffprobe.public_send m, *args, &block
  end

  def ffprobe
    @ffprobe ||= FFprobe::Build.new(@path)
  end

  def to_s
    path.to_s
  end

  alias_method :to_str, :to_s

  def to_arg
    "file:#{path}"
  end

  def ==(other)
    self.to_s == other.to_s
  end

  def name
    path.basename ".*"
  end

  def size
    (path.size? or 0).to_filesize
  end

  def ls_color
    Colorizeable::LsColors.call basename
  end

  def basename
    path.basename
  end

  def ext
    path.extname
  end
end

