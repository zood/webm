# frozen_string_literal: true

require_relative "classifier"
require_relative "thrower"

module InputFiles
  class Checker

    include Thrower

    def initialize(rule)
      @rule = rule
      @filter = rule.filter
      @amount = rule.amount
    end

    def verify(files)
      check_amount files if @amount
      check_types_and_counts files if @filter.any?
      return InputFile.new(files.last) if @rule.singular?
      files.map { |f| InputFile.new f }
    end

  private

    def check_amount(files)
      return if @amount.cover? files.size
      throw_amount_mismatch(@amount, files.size)
    end

    def check_types_and_counts(files)
      classified_files = Classifier.by_extension files
      check_type_shortage classified_files.keys
      classified_files.each { |type, quantity| validate type, quantity }
    end

    def validate(type, quantity)
      return if (boundaries = @filter.fetch type).cover? quantity
      throw_boundary_exceeding type, boundaries, quantity
    rescue KeyError
      throw_unexpected_type type
    end

    def check_type_shortage(types)
      return if (shortage = mandatory_types - types).empty?
      throw_absent_mandatory_type shortage
    end

    def mandatory_types
      @filter.keys.select { |type| !@filter[type].begin.zero? }
    end
  end
end
