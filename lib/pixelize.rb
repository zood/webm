# frozen_string_literal: true

class Pixelize
  attr_reader :pixels_per_side

  def initialize(pixels_per_side, minimal_side_length)
    @pixels_per_side = pixels_per_side || minimal_side_length
    @minimal_side_length = minimal_side_length
  end

  def active?
    pixel_size > 1
  end

  def to_s
    "scale=iw/%d:ih/%d,scale=%d*iw:%d*ih:flags=neighbor" %
      Array.new(4, pixel_size)
  end

  def bitrate_factor
    Math.sqrt(pixel_size)
  end

  def pixel_size
    @minimal_side_length / @pixels_per_side
  end
end
