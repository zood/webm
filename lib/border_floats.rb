# frozen_string_literal: true

class BorderFloats
  attr_reader :preceeding, :succeeding

  def initialize(sequence, position)
    determine_layers sequence, position
  end

  def determine_layers(sequence, position)
    sequence
      .slice_when { |i, j| i <= position and j >= position }.to_a
      .then do |slices|
        @preceeding, @succeeding = [slices.first.last, slices.last.first]
      end
  end
end
