# frozen_string_literal: true

module EditableMetadata
  attr_reader :edited_title

  def edit_single
    self.reset_edited_title
    edit self.to_s
  end

  def edit_bunch(titles)
    throw :halt, Color.err["No titles to edit"] if titles.empty?
    edit titles.uniq.join("\n")
  end

private

  def edit(title_string)
    tf = Tempfile.new
    File.write tf.path, title_string

    if system(ENV["EDITOR"], tf.path)
      @edited_title = tf.read.strip
    else
      throw :halt, Color.warn["Aborted"]
    end
  ensure
    tf.delete
  end
end
