# frozen_string_literal: true

module Subtitles
  module Extract
    module External
      class Args < DryRun
        include SRTFile

        def initialize(subs_file:, start_position:, seconds:, encoding:)
          populate do |i|
            i.push(
              "ffmpeg", "-y", "-loglevel", "error",
              "-sub_charenc", encoding, "-i", subs_file,
              "-c:s", "text", "-ss", start_position,
              "-t", seconds, srt_file.path
            )
          end
        end
      end
    end
  end
end
