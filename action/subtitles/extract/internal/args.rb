# frozen_string_literal: true

module Subtitles
  module Extract
    module Internal
      class Args < DryRun
        include SRTFile

        def initialize(container:, stream_id:, start_position:, seconds:)
          populate do |i|
            i.push(
              "ffmpeg", "-y", "-loglevel", "error", "-i", container,
              "-c:s", "text", "-ss", start_position, "-t", seconds,
              "-map", stream_id, srt_file.path
            )
          end
        end
      end
    end
  end
end
