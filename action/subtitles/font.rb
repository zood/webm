# frozen_string_literal: true

require_relative "../../lib/role/color_converter"

module Subtitles
  class Font
    include ColorConverter

    attr_reader :name, :size, :fg, :bg, :shadow, :outline, :alpha

    def initialize(args)
      @id = args.fetch(:id)
      @name = args.fetch(:name)
      @size = $opt.sub.size        || args[:size]    || 20
      @fg = rgb_to_bgr($opt.sub.fg || args[:fg]      || "ffefd5")
      @bg = rgb_to_bgr($opt.sub.bg || args[:bg]      || "000000")
      @shadow = $opt.sub.shadow    || args[:shadow]  || 0
      @outline = $opt.sub.outline  || args[:outline] || 1
      @alpha = _alpha
    end

  private

    def _alpha
      {1 => "33",
       2 => "66",
       3 => "99",
       4 => "cc"}.fetch($opt.sub.alpha, "00")
    end
  end

  module FontPreset
    extend self
    @@font_preset = {}

    def add(**args)
      @@font_preset.store args[:id], Font.new(args)
    end

    def get(default = :sans)
      @@font_preset.fetch $opt.sub.preset_id || default
    end
  end

  FontPreset.add(id: :cronos, name: "Cronos Pro")
  FontPreset.add(id: :sans, name: "Noto Sans")
  FontPreset.add(
    id: :arial,
    name: "Arial Black",
    fg: "fab000",
    shadow: 2,
    outline: 2
  )
  FontPreset.add(
    id: :cambria,
    name: "Cambria",
    shadow: 1,
    outline: 1
  )
end
