# frozen_string_literal: true

module DetectCrop
  class Args < DryRun
    def initialize(input_file:, position:)
      populate do |d|
        d.push(
          "ffmpeg", "-hide_banner", "-ss", position, "-i", input_file,
          "-vframes", "3", "-vf", "cropdetect=#{threshold}:2:0",
          "-f", "null", "/dev/null"
        )
      end
    end

  private

    def threshold
      {low: 16, med: 24, hi: 32}.fetch($opt.detect_crop, :med)
    end
  end
end
