# frozen_string_literal: true

module FFplay
  class Args < DryRun
    def initialize(**args)
      populate do |p|
        p.push(
          "ffplay", "-loglevel", "panic", "-ss", args[:start_position],
          "-i", args[:input_file], "-t", args[:duration], *_mute(args[:mute]),
          *args[:video_filter], "-loop", "0"
        )
      end
    end

  private

    def _mute(enable)
      return unless enable
      %w[-an]
    end
  end
end
