# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/filter/complex_filter"

describe ComplexFilter do
  describe "with one meaningful and one nil task" do
    before do
      @cf = ComplexFilter::Build.new
      @cf.add_task(:video, "0:v", "v") do |t|
        t << "foo"
        t << "bar"
      end

      @cf.add_task(:audio, "1:a", "a") do |t|
        t << nil
      end
    end

    it "should build the filter" do
      _(@cf.to_a).must_equal(
        %w[-filter_complex [0:v]foo,bar[v] -map [v] -map 1:a]
      )
    end
  end

  describe "with two meaningful task" do
    before do
      @cf = ComplexFilter::Build.new
      @cf.add_task(:video, "0:v", "v") do |t|
        t << "foo"
        t << "bar"
      end

      @cf.add_task(:audio, "1:a", "a") do |t|
        t << "baz"
        t << nil
      end
    end

    it "should build the filter" do
      _(@cf.to_a).must_equal(
        %w[-filter_complex [0:v]foo,bar[v];[1:a]baz[a] -map [v] -map [a]]
      )
    end
  end

  describe "with empty task" do
    before { @cf = ComplexFilter::Build.new }

    it "should return empty array" do
      _(@cf.to_a).must_equal []
    end
  end

  describe "task with nil filter" do
    before do
      @cf = ComplexFilter::Build.new
      @cf.add_task(:video, "0:v", "v") { |t| t << nil }
    end

    it "should return the mapped task only" do
      _(@cf.to_a).must_equal %w[-map 0:v]
    end
  end

  describe "adding map" do
    before do
      @cf = ComplexFilter::Build.new do |cf|
        cf.add_map("0:v")
        cf.add_task(:vol1, "0:a", "a0", nomap: true) { |t| t << "volume=1" }
        cf.add_task(:vol2, "1:a", "a1", nomap: true) { |t| t << "volume=2" }
        cf.add_task(:amix, %w[a0 a1], "a") do |t|
          t << "amix=inputs=2"
          t << "afade=foo"
        end
      end
    end

    it "should return the mapped task only" do
      _(@cf.to_a).must_equal(
        ["-filter_complex",
         "[0:a]volume=1[a0];[1:a]volume=2[a1];[a0][a1]amix=inputs=2,afade=foo[a]",
         "-map", "0:v", "-map", "[a]"]
      )
    end
  end
end
