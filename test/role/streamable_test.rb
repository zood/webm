# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/role/streamable"

StreamSpecifier = Class.new
StreamSpecifier.include Streamable

describe StreamSpecifier do
  before { @stream_specifier = StreamSpecifier.new }

  describe :zero_stream do
    it "should add leading '0:'" do
      _(@stream_specifier.zero_stream("foo")).must_equal "0:foo"
    end

    it "should keep the specifier as is" do
      _(@stream_specifier.zero_stream("1:foo")).must_equal "1:foo"
    end
  end

  describe :subtitle_stream do
    it "should add leading 's:'" do
      _(@stream_specifier.subtitle_stream("1")).must_equal "s:0"
    end

    it "should keep the specifier as is" do
      _(@stream_specifier.subtitle_stream("s:foo")).must_equal "s:foo"
    end
  end
end
