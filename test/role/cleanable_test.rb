# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/role/cleanable"

using Cleanable

describe Cleanable do
  describe :clean do
    it "empty input should return empty string" do
      _("".clean).must_equal ""
    end

    it 'should remove prefix ":file' do
      _("file:some title".clean).must_equal "some title"
    end
  end
end
