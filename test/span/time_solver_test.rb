# frozen_string_literal: true
# vim:ft=ruby:fdm=marker:

require "minitest/autorun"
require_relative "../../lib/span/time_solver"

describe TimeSolver do

  describe "explicit positions" do

    describe "should override implicit ones" do # {{{1
      before do
        @ts = TimeSolver.new(
          start: 10,
          implicit_start: proc { 20 },
          stop: 30,
          implicit_stop: 40
        )
      end

      it "should pass" do
        _(@ts.start).must_equal 10
        _(@ts.stop).must_equal 30
        _(@ts.duration).must_equal 20
      end
    end # }}}1

    describe "start and stop" do # {{{1
      before { @ts = TimeSolver.new(start: 10, stop: 25) }

      it "should pass" do
        _(@ts.start).must_equal 10
        _(@ts.stop).must_equal 25
        _(@ts.duration).must_equal 15
      end
    end # }}}1

    describe "start and duration" do # {{{1
      before { @ts = TimeSolver.new(start: 20, duration: 30) }

      it "should pass" do
        _(@ts.start).must_equal 20
        _(@ts.stop).must_equal 50
        _(@ts.duration).must_equal 30
      end
    end # }}}1

    describe "stop and sane duration" do # {{{1
      before { @ts = TimeSolver.new(stop: 70, duration: 40) }

      it "should pass" do
        _(@ts.start).must_equal 30
        _(@ts.stop).must_equal 70
        _(@ts.duration).must_equal 40
      end
    end # }}}1

    describe "stop and insane duration" do # {{{1
      before { @ts = TimeSolver.new(stop: 50, duration: 70) }

      it "should pass" do
        _(@ts.start).must_equal(-20)
        _(@ts.stop).must_equal 50
        _(@ts.duration).must_equal 70
      end
    end # }}}1

  end

end
