# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../init"
require_relative "../../lib/span/duration"

describe Duration do
  describe "negative duration" do
    it "should throw" do
      _(proc { Duration.new(-1) }).must_throw :halt
    end
  end

  describe :to_s do
    it "should return a full string" do
      _(Duration.new(10.123456).to_s).must_equal "00:00:10.123456"
    end

    it "should return a shorten string" do
      _(Duration.new(10.123456).to_s(true)).must_equal "00:10"
    end
  end
end
