# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/progress/progress_bar"

describe ProgressBar do
  before do
    @glyphs = " ▁▂▃▄▅▆▇█".chars
  end

  describe "progress bar with one slot" do
    before { @pb = ProgressBar.new @glyphs, 1 }

    it "should build empty bar" do
      _(@pb.draw(0)).must_equal ' '
    end

    it "should build half of the bar" do
      _(@pb.draw(0.5)).must_equal '▄'
    end

    it "should build full bar" do
      _(@pb.draw(1)).must_equal '█'
    end

    it "should build full bar" do
      _(@pb.draw(0.98)).must_equal '█'
    end
  end

  describe "progress bar with two slots" do
    before { @pb = ProgressBar.new @glyphs, 2 }

    it "should build empty bar" do
      _(@pb.draw(0)).must_equal '  '
    end

    it "should build half of the bar" do
      _(@pb.draw(0.5)).must_equal '█ '
    end

    it "should build full bar" do
      _(@pb.draw(1)).must_equal '██'
    end
  end

  describe "progress bar with three slots" do
    before { @pb = ProgressBar.new @glyphs, 3 }

    it "should build empty bar" do
      _(@pb.draw(0)).must_equal '   '
    end

    it "should build half of the bar" do
      _(@pb.draw(0.5)).must_equal '█▄ '
    end

    it "should build full bar" do
      _(@pb.draw(1)).must_equal '███'
    end
  end
end
