# frozen_string_literal: true

require "minitest/autorun"
require_relative "../lib/border_floats"

describe BorderFloats do
  before { @sequence = [10.0, 15.36, 20.48, 25.6, 30.72, 35.84] }

  describe "position with no tie" do
    before { @position = 20 }

    it "should get correct preceeding frame" do
      _(BorderFloats.new(@sequence, @position).preceeding)
        .must_equal 15.36
    end

    it "should get correct succeeding frame" do
      _(BorderFloats.new(@sequence, @position).succeeding)
        .must_equal 20.48
    end
  end

  describe "position with tie" do
    describe "sequence with no edges involved" do
      it "should get correct frames" do
        @sequence.each_with_index do |frame, idx|
          next if idx == 0 or idx == @sequence.size - 1

          surrounding_frames = BorderFloats.new(
            @sequence, frame
          )

          _(surrounding_frames.preceeding).must_equal @sequence[idx - 1]
          _(surrounding_frames.succeeding).must_equal @sequence[idx + 1]
        end
      end
    end
  end

  describe "tied keyframe of the leading edge" do
    it "should get correct frames" do
      surrounding_frames = BorderFloats.new(
        @sequence, @sequence.first
      )

      _(surrounding_frames.preceeding).must_equal @sequence.first
      _(surrounding_frames.succeeding).must_equal @sequence[1]
    end
  end

  describe "tied keyframe of the trailing edge" do
    it "should get correct frames" do
      surrounding_frames = BorderFloats.new(
        @sequence, 35.84
      )

      _(surrounding_frames.preceeding).must_equal 30.72
      _(surrounding_frames.succeeding).must_equal 35.84
    end
  end
end
