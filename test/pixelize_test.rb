# frozen_string_literal: true

require "minitest/autorun"
require_relative "../init"

describe Pixelize do
  describe :active? do
    it "should be false when pixels_per_side is Nil" do
      refute Pixelize.new(nil, 500).active?
    end

    it "should be true when pixels_per_side is not Nil" do
      assert Pixelize.new(42, 500).active?
    end
  end

  describe :pixel_size do
    it "should be 1 when pixels_per_side is Nil" do
      _(Pixelize.new(nil, 500).pixel_size).must_equal 1
    end

    it "should be greater than 1 when pixels_per_side is not Nil" do
      _(Pixelize.new(1, 500).pixel_size).must_be :>, 1
    end
  end
end
